﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Pages.Reviews
{
    public class DetailsModel : PageModel
    {
        private readonly ReviewService _reviewService;

        public DetailsModel(ReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        public Review Review { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Review = await _reviewService.GetSingle(id.GetValueOrDefault());

            if (Review == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
