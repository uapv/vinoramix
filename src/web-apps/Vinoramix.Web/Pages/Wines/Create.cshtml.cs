﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Wines
{
    public class CreateModel : PageModel
    {
        private readonly WineService _wineService;

        public CreateModel(WineService wineService)
        {
            _wineService = wineService;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Wine Wine { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _wineService.CreateAsync(Wine);

            return RedirectToPage("./Index");
        }
    }
}
