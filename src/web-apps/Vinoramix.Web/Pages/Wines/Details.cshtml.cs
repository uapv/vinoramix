﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using Vinoramix.Web.Models;
using Vinoramix.Web.Services;

namespace Vinoramix.Web.Wines
{
    public class DetailsModel : PageModel
    {

        private readonly WineService _wineService;

        public DetailsModel(WineService wineService)
        {
            _wineService = wineService;
        }

        public Wine Wine { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Wine = await _wineService.GetWine(id.GetValueOrDefault());

            if (Wine == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
