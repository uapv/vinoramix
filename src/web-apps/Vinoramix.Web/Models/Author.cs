﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Web.Models
{
    public class Author
    {
        public string Id { get; set; }

        public string Username { get; set; }
    }
}
