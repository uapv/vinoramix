﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Web.Models
{
    public class Review
    {
        public int Id { get; set; }

        public Author Author { get; set; }

        public float Note { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
