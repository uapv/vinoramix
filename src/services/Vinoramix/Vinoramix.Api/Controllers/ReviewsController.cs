﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Vinoramix.Api.Data;
using Vinoramix.Api.Data.Wines;
using Vinoramix.Api.Models.Users;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IUserRepository _userRepository;

        public ReviewsController(IReviewRepository reviewRepository, IUserRepository userRepository)
        {
            _reviewRepository = reviewRepository;
            _userRepository = userRepository;
        }

        // GET: api/Reviews
        [Authorize(Roles = UserRoles.User)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReviewGetDto>>> GetReviews()
        {
            var results = await _reviewRepository.GetAllAsync();

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return results
                .Select(review => new ReviewGetDto()
                {
                    Id = review.Id,
                    Author = new UserDto()
                    {
                        Id = review.Author.Id,
                        Username = review.Author.UserName
                    },
                    Note = review.Note,
                    Comment = review.Comment,
                    CreatedOn = review.CreatedOn
                }).ToList();
        }

        // GET: api/Reviews/5
        [Authorize(Roles = UserRoles.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ReviewGetDto>> GetWine(int id)
        {
            var review = await _reviewRepository.GetByIdAsync(id);

            if (review == null)
            {
                return NotFound();
            }

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return new ReviewGetDto()
            {
                Id = review.Id,
                Author = new UserDto()
                {
                    Id = review.Author.Id,
                    Username = review.Author.UserName
                },
                Note = review.Note,
                Comment = review.Comment,
                CreatedOn = review.CreatedOn
            };
        }

        // POST: api/Reviews
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = UserRoles.User)]
        [HttpPost]
        public async Task<ActionResult<Review>> PostReview(ReviewCreateDto review)
        {
            var username = User.Claims.First(c => c.Type == ClaimTypes.Name)?.Value;
            var authorId = _userRepository.GetUserIdByUsername(username);

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            var newReview = new Review()
            {
                WineId = review.WineId,
                AuthorId = authorId,
                CreatedOn = DateTime.Now,
                Note = review.Note,
                Comment = review.Comment
            };

            await _reviewRepository.CreateAsync(newReview);

            return Created(new Uri($"/api/wines/{newReview.WineId}", UriKind.Relative), null);
        }

        // DELETE: api/Reviews/5
        [Authorize(Roles = UserRoles.Administrator)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReviewGetDto>> DeleteReview(int id)
        {
            var review = await _reviewRepository.DeleteAsync(id);

            if (review == null)
            {
                return NotFound();
            }

            //TODO: Implémenter outil tel que AutoMapper au lieu mapper manuellement!
            return new ReviewGetDto()
            {
                Author = new UserDto() {
                    Id = review.Author?.Id,
                    Username = review.Author?.UserName
                },
                CreatedOn = review.CreatedOn,
                Note = review.Note,
                Comment = review.Comment
            };
        }
    }
}
