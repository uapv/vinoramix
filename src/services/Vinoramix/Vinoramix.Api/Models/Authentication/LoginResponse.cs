﻿namespace Vinoramix.Api.Models.Authentication
{
    public class RegisterResponse
    {
        public string Status { get; set; }

        public string Message { get; set; }
    }
}
