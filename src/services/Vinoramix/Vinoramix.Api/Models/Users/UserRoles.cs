﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vinoramix.Api.Models.Users
{
    public class UserRoles
    {
        public const string Administrator = "Administrator";
        public const string User = "User";
    }
}
