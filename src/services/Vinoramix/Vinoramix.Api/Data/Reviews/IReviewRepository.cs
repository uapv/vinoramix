﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public interface IReviewRepository
    {
        Task CreateAsync(Review review);

        Task<Review> GetByIdAsync(int id);

        Task<IList<Review>> GetAllAsync();

        Task<bool> UpdateAsync(Review review);

        Task<Review> DeleteAsync(int id);

        Task<bool> ExistsAsync(int id);
    }
}
