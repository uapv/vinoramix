﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Users;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data
{
    public static class DatabaseInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var users = await SeedUsersAsync(userManager, roleManager, context);
            _ = SeedWines(context, users);

            context.SaveChanges();
        }

        private static async Task<IList<User>> SeedUsersAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            if (context.Users.Any())
            {
                return Array.Empty<User>();
            }
            else
            {
                if (!await roleManager.RoleExistsAsync(UserRoles.Administrator))
                {
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Administrator));
                }

                if (!await roleManager.RoleExistsAsync(UserRoles.User))
                {
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.User));
                }

                var users = new List<User>();
                var user = new User()
                {
                    UserName = "user1",
                    Email = "user1@test.com",
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(user, "Password1$");
                await userManager.AddToRoleAsync(user, UserRoles.User);
                users.Add(user);

                user = new User()
                {
                    UserName = "admin1",
                    Email = "admin1@test.com",
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(user, "Password1$");
                await userManager.AddToRoleAsync(user, UserRoles.User);
                await userManager.AddToRoleAsync(user, UserRoles.Administrator);
                users.Add(user);

                return users;
            }
        }

        private static IList<Wine> SeedWines(ApplicationDbContext context, IList<User> users)
        {
            if (context.Wines.Any())
            {
                return Array.Empty<Wine>();
            }
            else
            {
                //new Wine()
                //{
                //  Name = "",
                //  GrapeVariety = "",
                //  VineyardName = "",
                //  Vintage = 0,
                //  Description = "",
                //  Price = 0.0
                //}

                var review = new Review()
                {
                    CreatedOn = new DateTime(2020, 01, 31),
                    Author = users.First(),
                    Note = 5,
                    Comment = "Le meilleur!"
                };
                context.Reviews.Add(review);

                var wines =
                    new List<Wine>()
                    {
                        new Wine()
                        {
                            Name = "Vino Uno",
                            GrapeVariety = "Cépage 1",
                            VineyardName = "Château d'Italia",
                            Vintage = 2020,
                            Description = "Jeune vin au bouquet plaisant.",
                            Price = 10.0M,
                            Reviews =
                                new List<Review>()
                                {
                                    review
                                }
                        },
                        new Wine()
                        {
                            Name = "2010 Domaine Cabassole Vacqueyras Tradition",
                            GrapeVariety = "Grenache - Mourvedre - Syrah",
                            VineyardName = "Domaine Cabassole",
                            Vintage = 2010,
                            Description = "Vin rouge de la région de Vacqueyras (Côtes du Rhône) en France.",
                            Price = 10.0M
                        },
                        new Wine()
                        {
                            Name = "2012 Château Pontet-Canet",
                            GrapeVariety = "Bordeaux",
                            VineyardName = "Château Pontet-Canet",
                            Vintage = 2012,
                            Description = "Vin rouge de la région de Pauillac (Medoc - Bordeaux) en France.",
                            Price = 84.0M
                        },
                        new Wine()
                        {
                            Name = "2016 Marques de Caceres Crianza",
                            GrapeVariety = "Tempranillo",
                            VineyardName = "Marques de Caceres",
                            Vintage = 2016,
                            Description = "Vin rouge de la région de Rioja Alta en Espagne.",
                            Price = 11.0M
                        },
                        new Wine()
                        {
                            Name = "2020 Boulaouane Cinsault Grenache",
                            GrapeVariety = "Cinsault Grenache",
                            VineyardName = "Boulaouane",
                            Vintage = 2020,
                            Description = "Vin gris de la région de Guerrouane au Maroc.",
                            Price = 10.0M
                        },
                        new Wine()
                        {
                            Name = "2019 Cellier des Chartreux La Nuit Tous Les Chats Sont Gris",
                            GrapeVariety = "Grenache",
                            VineyardName = "Cellier des Chartreux",
                            Vintage = 2019,
                            Description = "Vin rose du département de Gard en France.",
                            Price = 6.0M
                        },
                        new Wine()
                        {
                            Name = "2015 Dauvergne Ranvier Chateauneuf-du-Pape Grand Vin",
                            GrapeVariety = "Grenache - Syrah",
                            VineyardName = "Danvergne et Ranvier",
                            Vintage = 2015,
                            Description = "Vin rouge de la région de Châteauneuf-du-Pape (Vallée du Rhône) en France",
                            Price = 25.0M
                        }
                    };

                foreach(var wine in wines)
                {
                    context.Wines.Add(wine);
                }

                return wines;
            }
        }
    }
}
