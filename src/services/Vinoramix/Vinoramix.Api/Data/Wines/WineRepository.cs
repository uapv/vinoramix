﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public class WineRepository : IWineRepository
    {
        private readonly ApplicationDbContext _context;

        public WineRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IList<Wine>> GetSearchResultsAsync(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                return await GetAllAsync();
            }

            // Nettoyer la requête.
            var terms = query.Trim().ToLower().Split(";");

            // Trouver les vins qui match le premier terme de recherche.
            var results = _context.Wines
                .AsNoTracking()
                .Include(_ => _.Reviews)
                .ThenInclude(_ => _.Author)
                .Where(x =>
                    x.Name.ToLower().Contains(terms[0]) ||
                    x.GrapeVariety.ToLower().Contains(terms[0]) ||
                    x.VineyardName.ToLower().Contains(terms[0]) ||
                    x.Vintage.ToString().Contains(terms[0]));

            // Looper pour chacun des termes restants.
            for (int i = 1; i < terms.Length; i++)
            {
                // Variable temporaire nécessaire pour la requête LINQ suivant.
                var term = terms[i];

                // Faire une union entre les requêtes LINQ qui seront exécutées
                // par Entity Framework Core sur la base de données. Ceci permet
                // d'agréger tous les résultats positifs de façon distincte. Donc,
                // éviter les doublons!
                results = results
                    .Union(
                        _context.Wines
                            .AsNoTracking()
                            .Include(_ => _.Reviews)
                            .ThenInclude(_ => _.Author)
                            .Where(x =>
                                x.Name.ToLower().Contains(term) ||
                                x.GrapeVariety.ToLower().Contains(term) ||
                                x.VineyardName.ToLower().Contains(term) ||
                                x.Vintage.ToString().Contains(term))
                    );
            }

            return await results.ToListAsync();
        }

        public async Task<IEnumerable<Review>> GetWineReviewsAsync(int id)
        {
            return await _context.Reviews
                .AsNoTracking()
                .Where(_ => _.WineId == id)
                .Include(_ => _.Author )
                .OrderByDescending(_ => _.CreatedOn)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IList<Wine>> GetAllAsync()
        {
            return await _context.Wines
                .AsNoTracking()
                .Include(_ => _.Reviews)
                .ThenInclude(_ => _.Author)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Wine> GetByIdAsync(int id)
        {
            return await _context.Wines
                .AsNoTracking()
                .SingleOrDefaultAsync(_ => _.Id == id)
                .ConfigureAwait(false);
        }

        public async Task CreateAsync(Wine wine)
        {
            _context.Wines.Add(wine);
            await _context.SaveChangesAsync();
        }

        public async Task<Wine> DeleteAsync(int id)
        {
            var wine = await _context.Wines.FindAsync(id);

            if (wine == null)
            {
                return null;
            }

            _context.Wines.Remove(wine);
            await _context.SaveChangesAsync();

            return wine;
        }

        public Task<bool> ExistsAsync(int id)
        {
            return _context.Wines.AnyAsync(e => e.Id == id);
        }

        public async Task<bool> UpdateAsync(Wine wine)
        {
            _context.Entry(wine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await ExistsAsync(wine.Id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }
    }
}
