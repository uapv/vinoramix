﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vinoramix.Api.Models.Wines;

namespace Vinoramix.Api.Data.Wines
{
    public interface IUserRepository
    {
        string GetUserIdByUsername(string username);
    }
}
