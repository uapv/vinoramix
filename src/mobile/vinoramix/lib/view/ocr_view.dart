// Quentin RAYMONDAUD
// OCR View
// Presents almost all feature implemented for the Vinoramix Presentation.

import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';
import 'package:localstorage/localstorage.dart';
import 'package:vinoramix/httpservice/http_service.dart';
import 'package:vinoramix/httpservice/session_object.dart';
import 'package:vinoramix/httpservice/wine_object.dart';
import 'package:vinoramix/view/wine_details.dart';
import 'package:vinoramix/view/wine_item_view.dart';

import 'login_register_view.dart';

// Widget that old all the feature presentation view
class OCR extends StatefulWidget {
  @override
  _OCRState createState() => _OCRState();
}

// Parse Str Json and returns list<Wine>
List<Wine> parseJson(String response) {
  if (response == null) return [];
  final parsed = json.decode(response.toString()).cast<Map<String, dynamic>>();
  return parsed.map<Wine>((json) => new Wine.fromJson(json)).toList();
}

// Contenu du widget
class _OCRState extends State<OCR> {
  // Lien vers local storage
  final LocalStorage storage = new LocalStorage('todo_app');

  // List des vins scannées
  List<Wine> _scannedWines;

  // Caméra arrière par défaut
  int _cameraOcr = FlutterMobileVision.CAMERA_BACK;

  // Flash désactivé par défaut
  bool _flash = false;
  IconData _flashIcon = Icons.flash_off;

  // Texte qui change d'état en fonction de l'etat du flash
  String _textButton = '📷 Scan';

  // Session property loaded from localstorage when sending query
  Session _session;

  // Result get from http
  Wine _scanResult;

  // Query textfield controller
  final TextEditingController _queryFilter = new TextEditingController();
  String _query = "";

  // Query textfield listener function
  void _queryListen() {
    if (_queryFilter.text.isEmpty) {
      _query = "";
    } else {
      _query = _queryFilter.text;
    }
  }

  // Constructor that bind controller & listener function
  _OCRState() {
    _queryFilter.addListener(_queryListen);
    if (storage != null) {
      var s = storage.getItem("ScannedWines");
      _scannedWines = (s != null)
          ? Wine.winesFromJson(json.decode(json.encode(s)))
          : List<Wine>();
    } else
      _scannedWines = List<Wine>();
  }

  // Handle Android back button action on main page
  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text("NO"),
              ),
              SizedBox(height: 16),
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(true),
                child: Text("YES"),
              ),
            ],
          ),
        ) ??
        false;
  }

  // Widget content
  // Should be splitted into subclasses
  @override
  Widget build(BuildContext context) {
    if (storage != null) if (storage.getItem("session") != null)
      return new MaterialApp(
          theme: new ThemeData(
            primarySwatch: Colors.amber,
            buttonColor: Colors.amberAccent,
          ),
          home: WillPopScope(
            onWillPop: _onBackPressed,
            child: new Scaffold(
                appBar: new AppBar(
                  title: new Text('Vinoramix'),
                  centerTitle: true,
                ),
                body: new Container(
                  color: Colors.grey[300],
                  constraints: BoxConstraints.expand(),
                  child: SingleChildScrollView(
                      child: new Padding(
                    padding: EdgeInsets.all(30),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                            margin: EdgeInsets.all(5),
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: ButtonTheme(
                                    height: 50,
                                    child: RaisedButton(
                                        child: new Text(_textButton,
                                            style: new TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black)),
                                        onPressed: clearAndRead,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(40),
                                            side: BorderSide(
                                                color: Colors.amberAccent)))),
                              ),
                              IconButton(
                                icon: Icon(_flashIcon),
                                tooltip: "Switch on off the flash",
                                onPressed: () {
                                  setState(() {
                                    changeFlash();
                                  });
                                },
                              )
                            ])),

                        Container(
                          margin: EdgeInsets.all(5),
                          child: new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side:
                                    BorderSide(width: 0.1, color: Colors.black),
                              ),
                              color: Colors.grey[400],
                              child: Container(
                                child: new Column(children: <Widget>[
                                  Text(
                                    'Result 📷',
                                    style: new TextStyle(
                                      fontSize: 22.0,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  new Row(children: <Widget>[
                                    Expanded(
                                        child: Container(
                                            height: 325,
                                            // constraints: BoxConstraints.expand(),
                                            // Use future builder and DefaultAssetBundle to load the local JSON file
                                            child: new FutureBuilder(
                                                builder: (context, snapshot) {
                                              return !(_scanResult == null)
                                                  ? new WinePage(_scanResult,
                                                      header: false)
                                                  : new Stack(
                                                      children: <Widget>[
                                                          Center(
                                                              child:
                                                                  CircularProgressIndicator()),
                                                          Align(
                                                              child: Text(
                                                                "Scannez un vin !",
                                                                style:
                                                                    new TextStyle(
                                                                  fontSize:
                                                                      20.0,
                                                                  color: Colors
                                                                      .black54,
                                                                ),
                                                              ),
                                                              alignment: Alignment
                                                                  .bottomCenter)
                                                        ]);
                                            }))),
                                  ]),
                                ]),
                              )),
                        ),

                        Container(
                            margin: EdgeInsets.all(5),
                            child: Row(children: [
                              IconButton(
                                  icon: Icon(Icons.search),
                                  tooltip: "Request Server",
                                  onPressed: _doQuery),
                              Expanded(
                                child: TextField(
                                    controller: _queryFilter,
                                    onSubmitted: (value) => _doQuery,
                                    decoration: new InputDecoration(
                                        labelText: 'Query',
                                        //prefixIcon: Icon(Icons.search),
                                        border: new OutlineInputBorder(
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(40.0),
                                          ),
                                        ))),
                              ),
                            ])),

                        // Scan Result

                        // SAVED
                        new Card(
                            margin: EdgeInsets.all(5),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(width: 0.1, color: Colors.black),
                            ),
                            color: Colors.grey[400],
                            child: Container(
                              margin: EdgeInsets.all(5),
                              child: new Column(children: <Widget>[
                                Text(
                                  'Previous 📷',
                                  style: new TextStyle(
                                    fontSize: 22.0,
                                    color: Colors.black54,
                                  ),
                                ),
                                new Row(children: <Widget>[
                                  Expanded(
                                      child: Container(
                                          height: 250,
                                          // Use future builder and DefaultAssetBundle to load the local JSON file
                                          child: new FutureBuilder(
                                              builder: (context, snapshot) {
                                            return (_scannedWines != null &&
                                                    _scannedWines.isNotEmpty)
                                                ? new WineList(
                                                    wine: _scannedWines)
                                                : new Stack(children: <Widget>[
                                                    Center(
                                                        child:
                                                            LinearProgressIndicator()),
                                                    Align(
                                                        child: Text(
                                                          "Votre historique de scan est vide !",
                                                          style: new TextStyle(
                                                            fontSize: 20.0,
                                                            color:
                                                                Colors.black54,
                                                          ),
                                                        ),
                                                        alignment: Alignment
                                                            .bottomCenter)
                                                  ]);
                                          }))),
                                ]),
                              ]),
                            )),
                      ],
                    ),
                  )),
                )),
          ));
    else
      return LoginPage();
  }

  // Calls http service for scanWine with view & localstorage data and store response as _scanResult
  Future<Wine> _doQuery() async {
    setState(() {
      this._scanResult = null;
    });

    if (storage != null && storage.getItem('session') != null)
      _session = Session.fromJson(
          storage.getItem('session'), storage.getItem('session')['username']);
    Wine res = await HttpService.scanWine(
        Uri.encodeFull(_query), storage.getItem("ipport"));
    setState(() {
      _scanResult = res;
    });

    if (_scanResult == null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
      return null;
    } else {
      if (null ==
              _scannedWines.firstWhere((item) => item.id == _scanResult.id,
                  orElse: () => null) &&
          _scanResult.id != -1) _scannedWines.add(res);
      return _scanResult;
    }
  }

  // Enbaling/Diabling Flash listener
  void changeFlash() {
    _flash = !_flash;
    if (_flash) {
      _flashIcon = Icons.flash_on;
      _textButton = '📸 Scan';
    } else {
      _flashIcon = Icons.flash_off;
      _textButton = '📷 Scan';
    }
  }

  // Open Camera Scanner
  Future<void> clearAndRead() async {
    this._queryFilter.text = "";
    await _read();
    _doQuery();
  }

  // Parse Results from camera scanner & retrieve it in textfield
  Future<Null> _read() async {
    List<OcrText> texts = [];
    try {
      texts = await FlutterMobileVision.read(
          camera: _cameraOcr,
          flash: _flash,
          autoFocus: true,
          multiple: true,
          waitTap: true,
          showText: true,
          fps: 2.0);

      setState(() {
        for (var i = 0; i < texts.length; i++) {
          if (i != 0) _queryFilter.text += ";";
          _queryFilter.text += texts[i]
              .value
              .replaceAll(",", "")
              .replaceAll(" ", ";")
              .replaceAll("\n", ";")
              .replaceAll(";;", ";");
        }
        if (_queryFilter.text != null &&
            _queryFilter.text.substring(_queryFilter.text.length - 2) != ";")
          _queryFilter.text += ";";
        _queryFilter.text = _queryFilter.text;
        //_textValue = texts[0].value;
      });
    } on Exception {
      texts.add(new OcrText('Failed to recognize text.'));
    }
  }
}
