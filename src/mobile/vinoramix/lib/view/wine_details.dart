// Quentin RAYMONDAUD
// Wine details minimal view

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:vinoramix/httpservice/http_service.dart';
import 'package:vinoramix/httpservice/review_object.dart';
import 'package:vinoramix/httpservice/session_object.dart';
import 'package:vinoramix/httpservice/wine_object.dart';

// Widget that displays a specific Wine in detail
class WinePage extends StatefulWidget {
  Wine subject;
  bool header;

  WinePage(Wine w, {header: true}) {
    this.subject = w;
    this.header = header;
  }

  @override
  State<StatefulWidget> createState() =>
      new WineDetails(this.subject, this.header);
}

// Widget Content
class WineDetails extends State<WinePage> {
  final LocalStorage storage = new LocalStorage('todo_app');
  Session _session;
  BuildContext context;
  final TextEditingController _descriptionFilter = new TextEditingController();
  String _description = "";
  Wine subject;
  int _currentRate = 3;
  bool header;
  final _formKey = GlobalKey<FormState>();

  // Constructor
  // Header = true displays Vinoramix MaterialApp
  // Header = false not only card content ( ie: displaying in listview )
  WineDetails(Wine subject, bool header) {
    this.subject = subject;
    this.header = header;
    _descriptionFilter.addListener(_descriptionListen);
  }

  // Comment textfield listener
  _descriptionListen() {
    if (_descriptionFilter.text.isEmpty) {
      _description = "";
    } else {
      _description = _descriptionFilter.text;
    }
  }

  // Content builder ( Add review dialog & Wine + reviews )
  @override
  Widget build(BuildContext context) {
    this.context = context;
    if (this.header)
      return new Scaffold(
        appBar: _buildBar(context),
        body: new Container(
            color: Colors.grey[300],
            padding: EdgeInsets.all(30),
            constraints: BoxConstraints.expand(),
            child: detailBody()),
      );
    else
      return detailBody();
    throw UnimplementedError();
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      title: new Text("Vinoramix"),
      centerTitle: true,
    );
  }

  // Shared content with header = false & header = true
  Widget detailBody() {
    if (this.subject.id == -1)
      return new SizedBox(
          height: 500,
          child: Column(children: <Widget>[
            Column(children: [
              Text(
                '🤷‍♂️' + this.subject.name + '🤷‍♂️',
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              ),
              new Padding(
                  padding: EdgeInsets.all(30),
                  child: Text(
                    "Sorry !\n" +
                        "We couldn't recognize this Wine yet!\n\n" +
                        "This wine is probably fantastic ! \nSo let us taste, let us know !",
                    style: new TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.center,
                  )),
              new RaisedButton(child: Text("😞 Send Picture & informations 😞"))
            ])
          ]));
    return SingleChildScrollView(
        child: Container(
            margin: ((!header)
                ? EdgeInsets.all(20)
                : EdgeInsets.all(5)),
            child: new SizedBox(
                height: ((!header) ? 400 : 700),
                child: Column(children: <Widget>[
                  Column(children: [
                    Text(
                      this.subject.name,
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(this.subject.description),
                    Container(
                        decoration: deco(),
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        child: ButtonBar(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text("Cuvée: " + this.subject.vintage.toString()),
                              Text(
                                "Cépage: " + this.subject.grapeVariety,
                              ),
                              Text("Vineyard: " + this.subject.vineyardName),
                              Text("Price: " +
                                  this.subject.price.toString() +
                                  "€")
                            ])),
                  ]),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        RaisedButton(
                            child: Text("Add Review"),
                            onPressed: () => showDialog(
                                  context: context,
                                  builder: (context) {
                                    String contentText = "Rate: " +
                                        this._currentRate.toString() +
                                        "/5";
                                    var height =
                                        MediaQuery.of(context).size.height;
                                    var width =
                                        MediaQuery.of(context).size.width;

                                    return StatefulBuilder(
                                      builder: (context, setState) {
                                        return AlertDialog(
                                          title: Text("Your review"),
                                          content: Container(
                                            height: height / 3,
                                            width: width - 100,
                                            child: new TextField(
                                              controller: _descriptionFilter,
                                              maxLines: 15,
                                              decoration: new InputDecoration(
                                                  labelText: 'Comment',
                                                  prefixIcon:
                                                      Icon(Icons.comment),
                                                  border: OutlineInputBorder(),
                                                  isDense: true),
                                            ),
                                          ),
                                          actions: <Widget>[
                                            Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Text(contentText),
                                                FlatButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      this._currentRate =
                                                          _currentRate > 0
                                                              ? _currentRate -=
                                                                  1
                                                              : _currentRate =
                                                                  0;
                                                      contentText = "Rate: " +
                                                          this
                                                              ._currentRate
                                                              .toString() +
                                                          "/5";
                                                    });
                                                  },
                                                  child: Text("-1"),
                                                ),
                                                FlatButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      this._currentRate =
                                                          _currentRate < 5
                                                              ? _currentRate +=
                                                                  1
                                                              : _currentRate =
                                                                  5;
                                                      contentText = "Rate: " +
                                                          this
                                                              ._currentRate
                                                              .toString() +
                                                          "/5";
                                                    });
                                                  },
                                                  child: Text("+1"),
                                                ),
                                              ]),
                                              Row(
                                                children: [
                                                  FlatButton(
                                                      onPressed: () =>
                                                          Navigator.pop(
                                                              context),
                                                      child: Text("Cancel"),
                                                      color: Colors.black),
                                                  RaisedButton(
                                                    color: Colors.amber,
                                                    onPressed: () {
                                                      addRev();
                                                      Navigator.pop(context);
                                                    },
                                                    child: Text("Submit"),
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2.0),
                                                        side: BorderSide(
                                                            color: Colors
                                                                .amberAccent)),
                                                  )
                                                ],
                                              )
                                            ])
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ))
                      ]),
                  Expanded(
                      child: Padding(
                          padding: ((!header)
                              ? EdgeInsets.all(5)
                              : EdgeInsets.all(5)),
                          child: ListView.builder(
                              itemCount: this.subject.reviews == null
                                  ? 0
                                  : this.subject.reviews.length,
                              itemBuilder: (BuildContext context, int index) {
                                return new Container(
                                    margin: EdgeInsets.all(5),
                                    decoration: new BoxDecoration(
                                      boxShadow: [
                                        new BoxShadow(
                                          color: Colors.amber.withOpacity(0.3),
                                          blurRadius: 2.0,
                                        ),
                                      ],
                                    ),
                                    child: Card(

                                        //margin: EdgeInsets.all(5),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(2.0),
                                          side: BorderSide(
                                              width: 0.3,
                                              color: Colors.black54),
                                        ),
                                        child: Column(children: <Widget>[
                                          Container(
                                              child: Text("Review:  " +
                                                  this
                                                      .subject
                                                      .reviews[index]
                                                      .comment),
                                              padding: EdgeInsets.all(10)),
                                          Container(
                                              child: Text("Rate:  " +
                                                  this
                                                      .subject
                                                      .reviews[index]
                                                      .note
                                                      .toString()+"/5"),
                                              padding: EdgeInsets.all(10))
                                        ])));
                              })))
                ]))));
  }

  // Golden square around a container
  BoxDecoration deco() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.amber, //                   <--- border color
        width: 5.0,
      ),
    );
  }

  // Call HTTPSERVICE with data provided in AddReview dialog
  addRev() async {
    print("addReview()");
    await HttpService.addReview(
        _description, _currentRate, this.subject.id, storage.getItem('ipport'));

    if (storage != null && storage.getItem('session') != null)
      _session = Session.fromJson(
          storage.getItem('session'), storage.getItem('session')['username']);
    List<Review> res = await HttpService.getReviews(
        this.subject.id.toString(), storage.getItem('ipport'));
    setState(() {
      this.subject.reviews = res;
    });
    return res;
  }
}
