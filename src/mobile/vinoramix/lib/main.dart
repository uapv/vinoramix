// RAYMONDAUD Quentin
// Flutter App main

import 'package:flutter/material.dart';
import 'package:vinoramix/view/ocr_view.dart';
import 'view/login_register_view.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Vinoramix',
      theme: new ThemeData(
          primarySwatch: Colors.amber
      ),
      home: new OCR(),
    );
  }
}

