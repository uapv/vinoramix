// RAYMONDAUD Quentin
// Vinoramix Wine object class

import 'package:vinoramix/httpservice/review_object.dart';

// Wine Data structure
class Wine{
  int id;
  int vintage;
  String name;
  String description;
  String grapeVariety;
  String vineyardName;
  double price;
  List<Review> reviews;

  Wine({this.id = -1,
        this.vintage = 0,
        this.name = "Please try again",
        this.description = "Unknown",
        this.grapeVariety= "Unknown",
        this.vineyardName=  "Unknown",
        this.price = -1,
        this.reviews });




  // Parse Map<> and returns a Wine
  factory Wine.fromJson(Map<String, dynamic> json){
    if ( json != null ) {
      List<Review> revs = Review.ReviewsFromJson(json["reviews"]);
      return Wine(
          id: json["id"],
          vintage: json["vintage"],
          name: json["name"],
          description: json["description"],
          grapeVariety: json["grapeVariety"],
          vineyardName: json["vineyardName"],
          price: json["price"],
          reviews: revs
      );
    }
    else
      return null;
  }

  static List<Wine> winesFromJson(jsonDecode) {
    List<Wine> wines = new List<Wine>();
    for(final w in jsonDecode){
      wines.add(Wine.fromJson(w));
    }
    return wines;
  }

  /* Parse Map<> and returns a Wine
  toJson(Map<String, dynamic> json){
    if ( json != null ) {
      List<Review> revs = Review.toJson(reviews);
      Map<String,dynamic> json = new Map<String,dynamic>();
      json["id"] = id;
      json["vintage"] = vintage;
      json["name"] = name;
      json["description"] = description;
      json["grapeVariety"] = grapeVariety;
      json["grapeVariety"] = vineyardName;
      json["price"] = price;
      json["reviews"] = revs;
    }
    else
      return null;
  }*/
}
