// Quentin RAYMONDAUD
// Http services Static Class

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:vinoramix/httpservice/review_object.dart';
import 'package:vinoramix/httpservice/session_object.dart';
import 'package:vinoramix/httpservice/wine_object.dart';

// Class that handle backend interactions
class HttpService{

  static Session _session; // Current Session that contains token used for backend request

  // Request the backend and returns a Session Object if succeed
  static Future<Session> login(String username,
                        String password,
                        String ipport) async {
    try {
       http.Response response = await http.post(
        'http://'+ipport+'/api/auth/login/',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': username,
          'password': password
        }),
      );

      if (response.statusCode == 200) {
        // If the server did return a 200 CREATED response,
        // then parse the JSON.
        print(response.body);
        _session = Session.fromJson(json.decode(response.body),username);
        return _session;
      } else {
        print(response.body);
        // If the server did not return a 201 CREATED response,
        // then throw an exception.
        return null;
        //throw Exception('Failed to load album');
      }
    }
    catch(e){
      print("MOCK MODE ! " + e.toString());
      return null;
      // MOCK TEST
      /*Map<String,dynamic> mock = new Map<String,dynamic>();
      mock["token"]= "fakeFlutterToken";
      mock["expiration"]= "fakeFlutterExpiration";
      return Session.fromJson(mock,username);*/
      // FIN MOCK TEST
    }
  }

  // Requests the backend for creating a new user.
  // Returns JSON response body
  static Future<Map<String, dynamic>> register(String username,
                                 String password,
                                 String email,
                                 String ipport) async {
    try {
      http.Response response = await http.post(
        'http://'+ipport+'/api/auth/register',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': username,
          'password': password,
          'email':email
        }),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 CREATED response,
        // then parse the JSON.
        print("True" + response.body);
        return json.decode(response.body);
      } else {
        print("False" + response.body);
        // If the server did not return a 201 CREATED response,
        // then throw an exception.
        return json.decode(response.body);
        //throw Exception('Failed to load album');
      }
    }
    catch(e) {
      print(e);
      return null;
    }
  }

  // Request the backend for adding a review to a wine
  static Future<http.Response> addReview(
                          String comment,
                          int rate,
                          int wineId,
                          String ipport)async {
    try {
      http.Response response = await http.post(
        'http://' + ipport + '/api/reviews',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': ('Bearer ' + _session.token)
        },
        body: jsonEncode(<String, dynamic>{
          'wineId': wineId,
          'note': rate,
          'comment': comment,
        }),
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 CREATED response,
        // then parse the JSON.
        print(response.statusCode);
        return response;
      } else {
        print(response.statusCode);
        // If the server did not return a 201 CREATED response,
        // then throw an exception.
        return response;
        //throw Exception('Failed to load album');
      }
    }
    catch(e) {
      print(e);
      return null;
    }
  }

  // Request the backend for get a Wine from a SEMICOLONDELIMITED query sent into get request query field
  static Future<Wine> scanWine(String search, String ipport) async {

    try {
      final response = await http.get(
          'http://' + ipport + '/api/wines/top?query=' + search,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': ('Bearer ' + _session.token)
          }
      );
      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        print(response.body);
        return Wine.fromJson(jsonDecode(response.body));
      }
      else if ( response.statusCode == 204){
        return Wine();
      }
      else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        print(response.statusCode);
        throw Exception('Failed to load Wine');
      }
    }
    catch(e){
      print(e);
      return null;
    }

  }

  // Request the backend for get a Wine with id = "id"
  static Future<List<Review>> getReviews(String id, String ipport) async {
    final response = await http.get(
        'http://' + ipport +'/api/wines/'+id+"/reviews",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': ('Bearer ' + _session.token)
        }
    );
    print(response.body);
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.body);
      return Review.ReviewsFromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      print(response.body);
      throw Exception('Failed to load album');
    }
  }

  // Request the backend for get a Wine with id = "id"
  static Future<Wine> getWine(String id, String ipport) async {
    final response = await http.get(
        'http://' + ipport +'/api/wines/'+id,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': ('Bearer ' + _session.token)
        }
    );
    print(response.body);
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.body);
      return Wine.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      print(response.body);
      throw Exception('Failed to load album');
    }
  }
}

